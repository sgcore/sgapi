<?php
namespace models;
use Illuminate\Database\Eloquent\Model;
class Tag extends Model
{
    protected $fillable = [
        'id','tag', 'description', 'private'
    ];
    protected $visible = 
        ['id', 'tag', 'description', 'private'];
    
    
   
}
