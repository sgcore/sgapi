<?php
namespace models;
use Illuminate\Database\Eloquent\Model;
class Office extends Model
{
    protected $fillable = [
        'name', 'address', 'code',
    ];
}
