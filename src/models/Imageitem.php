<?php
namespace models;
use Illuminate\Database\Eloquent\Model;
class Imageitem extends Model
{
    protected $fillable = [
        'name', 'description', 'target', 'item_id'
    ];
    protected $visible = [
        'id',
        'name',
        'description',
        ];
    
    public function product()
    {
      return $this->belongsTo(Item::class);
    }
}
