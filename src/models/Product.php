<?php
namespace models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\softDeletes;
class Product extends Model
{
   // use softDeletes;
    use DynamicHiddenVisible;
    protected $table = 'gcobjetos';
    protected $appends = ['Precio', 'name', 'price', 'code'];
    protected $visible = ['Nombre', 'Codigo'];
    protected $fillable = ['image'];

    /**
     * Muestra lo basico agregado el costo y la ganancia
     */
    public static function showCosto($owner)
    {
        self::showBasic();
        if($owner)
            self::showColums(['Costo', 'Ganancia']);
    }

    /**
     * Solo agrega el precio iva y dolar sin lo basico
     */
    public static function showPrecio()
    {
      self::showColums(['id', 'Nombre', 'Codigo','Precio', 'Iva', 'Dolar']);
    }

    /**
     * Muestra lo basico, incluye el precio (sin el costo)
     */
    public static function showBasic()
    {
        self::showPrecio();
        self::showColums(['Descripcion', 'Stock', 'image', 'updated_at']);
    }

    /**
     * Muestra lo basico e informacion adicional del producto (sin el costo ni las caracteristicas)
     */
    public static function showExtra()
    {
        self::showBasic();
      self::showColums(['Serial', 'Link', 'Grupo', 'State', 'Metrica', 'Ideal']);
    }
    
    /**
     * Muestra todo menos el costo
     */
    public static function showCaract()
    {
        self::showExtra();
      self::showColums(['Cara0', 'Cara1', 'Cara2', 'Cara3',
                                'Cara4', 'Cara5', 'Cara6', 'Cara7',
                                'Cara8', 'Cara9']);
    }
    /**
     * Muestra todo
     */
    public static function showAll($owner)
    {
        self::showCosto($owner);
        self::showCaract();
        
        self::showColums(['Decendents', 'Subproducts', 'Subcategories']);

    }
    /**
     * Muestra para eapi
     */
    public static function showEapi()
    {
        self::showColums(['id', 'code', 'name', 'price','image']);

    }

    public function scopeCategories($q)
    {
       return $q->where('tipo', 1);
    }
    public function scopeProducts($q)
    {
       return $q->where('tipo', 0);
    }
    
    public function scopeRoots($q)
    {
       return $q->where(['tipo'=>1, 'parent'=> 0]);
    }
    public function subitems()
    {
        return $this->hasMany('models\Product', 'Parent' ,'Id');
    }
    public function parent()
    {
        return $this->belongsTo('models\Product', 'Parent');
    }
    public function Subcategories()
    {
        return $this->subitems()->categories()->with('Subcategories');
    }
    public function Decendents()
    {
        return $this->subitems()->with('Decendents');
    }
    public function Subproducts()
    {
        return $this->subitems()->products();
    }
    public function getPrecioAttribute()
    {
        $costo = \str_replace(",",".",$this->Costo);
        $ganancia = \str_replace(",",".",$this->Ganancia);
        $price = $costo + ($costo * $ganancia  / 100);
        $ret =  floor($price * 100) / 100;
        return $ret;
    }
    public function getPriceAttribute()
    {
        return $this->Precio;
    }
    public function getNameAttribute()
    {
        return $this->Nombre;
    }
    public function getCodeAttribute()
    {
        return $this->Codigo;
    }
    public function UpdateFromRequest($request)
    {
        $updated = $request->request->get("updated_at");
        $time = 0;
        if ($updated) {
            $datetime1 = new \DateTime($updated);
            $datetime2 = new \DateTime($this->updated_at);
            $interval = $datetime2->diff($datetime1);
            $time = $interval->s;
          }
        if($time > 0)
        {
            $this->Nombre = $request->request->get("Nombre");
            $this->Descripcion = $request->request->get("Descripcion");
            $this->Serial = $request->request->get("Serial");
            $this->Link = $request->request->get("Link");
            $this->Costo = \str_replace(",",".",$request->request->get("Costo")) * 1;
            $this->Ganancia = \str_replace(",",".",$request->request->get("Ganancia")) * 1;
            $this->Cara0 = $request->request->get("Cara0");
            $this->Cara1 = $request->request->get("Cara1");
            $this->Cara2 = $request->request->get("Cara2");
            $this->Cara3 = $request->request->get("Cara3");
            $this->Cara4 = $request->request->get("Cara4");
            $this->Cara5 = $request->request->get("Cara5");
            $this->Cara6 = $request->request->get("Cara6");
            $this->Cara7 = $request->request->get("Cara7");
            $this->Cara8 = $request->request->get("Cara8");
            $this->Cara9 = $request->request->get("Cara9");
            $this->Parent = $request->request->get("Parent")  * 1;
            $this->Tipo = $request->request->get("Tipo") * 1;
            $this->Iva = \str_replace(",",".",$request->request->get("Iva")) * 1;
            $this->State = $request->request->get("State")  * 1;
            $this->Metrica = $request->request->get("Metrica")  * 1;
            $this->Dolar = \str_replace(",",".",$request->request->get("Dolar")) * 1;
            $this->Ideal = \str_replace(",",".",$request->request->get("Ideal")) * 1;
            $this->Stock = \str_replace(",",".",$request->request->get("Stock")) * 1;
            return true;
        }
        return false;
        
    }
}
