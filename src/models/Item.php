<?php
namespace models;
use Illuminate\Database\Eloquent\Model;
class Item extends Model
{
    protected $fillable = [
        'name', 'description', 'code', 'image'
    ];
    public function images()
    {
        return $this->hasMany(Imageitem::class);
    }
    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

}
