<?php
namespace models;
use Illuminate\Database\Eloquent\Model;
use clientes\Cliente;
class Negocio extends Model
{
  Protected $primaryKey = "Id";
  protected $fillable = [
    'Nombre',
    'Direccion',
    'Telefono',
    'Slogan',
    'Mail'];
    public function saveImage($simg){
      if(substr( $simg, 0, 4 ) === "data") {
        list($type, $data) = explode(';', $simg);
        list(, $data)      = explode(',', $data);
        if($data){
          $img = imagecreatefromstring(base64_decode($data));
          if (!$img) {
              return false;
          }
          $imgname = Cliente::randomString($this->Id).'.png';
          $path =Cliente::getImageFolder().'/'.$imgname;
          imagepng($img, $path);
          $info = getimagesize($path);

          if ($info[0] > 0 && $info[1] > 0 && $info['mime']) {
            $this->image = $imgname;
            return true;
          }
          $this->image = '';
          unlink($path);
        }

      }
      return false;

    }
    public function generateCode(){
      $this->Codigo = Cliente::randomString($this->Id);
    }
    public function getFolderPath(){
      $this->Codigo = Cliente::getNegocioPath($this);
    }
    public function cleanMe() {
      $mask = "*.jpg";
      array_map( "unlink", glob( $mask ) );
    }
}
