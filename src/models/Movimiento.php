<?php
namespace models;
use Illuminate\Database\Eloquent\Model;
class Movimiento extends Model
{
    protected $table = 'gcmovimientos';
    protected $appends =['Total','Pagado'];
    protected $visible = [
      'Fecha',
      'Observaciones',
      'Caja',
      'Tipo',
      'Cliente',
      'Pagos',
      'Movidos'

      ];
    public function Movidos()
    {
        return $this->hasMany('models\Movido', 'Movimiento' ,'Id');
    }
    public function Pagos()
    {
        return $this->hasMany('models\Pago', 'Movimiento' ,'Id');
    }
    public function Cliente()
    {
      return $this->belongsTo('models\Destinatario',$this->_getDestino(), 'Id');
    }
    public function getTotalAttribute()
    {
       return $this->Movidos()->sum('Monto');
    }
    public function getPagadoAttribute()
    {
       return $this->Pagos()->sum('Monto');
    }
    private function _getDestino()
    {
        switch ($this->Tipo)
        {
            case 0://..Compra:
            case 3://..Devolucion:
            case 5://..Entrada:
            case 6://..Deposito:
            case 9://.Pedido:
                return 'Origen';

            case 2://..Venta:
            case 4://..Salida:
            case 1://..Retorno:
            case 7://..Extraccion:
            case 8://.Presupuesto:
                return 'Destino';
        }
        return $this->Origen;

    }

}
