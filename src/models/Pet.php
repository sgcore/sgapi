<?php

namespace models;

use Illuminate\Database\Eloquent\Model;
class Pet extends Model
{
  protected $appends = ['nextPicPrice', 'picsCount', 'bones', 'reports', 'price', 'canDelete', 'canAddPic' ];
  private $picsCount = 0;
  private $nextPicPrice = 0;
  private $priceCollected = 0;
  private $bonesCollected = 0;
  private $reportsCollected = 0;
  private $canDelete = false;
  private $updated = false;
  private $userMoney = 0;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'description'
    ];
    function updateStatus() {
        $this->updated = true;
        $pics = $this->pics;
        $this->picsCount = 0;
        $this->nextPicPrice = 0;
        $this->canDelete = true;
        $this->priceCollected = 0;
        $this->bonesCollected = 0;
        $this->reportsCollected = 0;
        foreach ($pics as $p) {
            $this->picsCount++;
            $this->nextPicPrice += pow(4, $this->picsCount);
            $this->canDelete &= $this->user_id === $p->user_id;
            $this->priceCollected += $p->price ;
            $this->bonesCollected =  $p->bones;
            $this->reportsCollected = $p->reports;
        }
        $this->updated = true;
        return $this;
        
    }
    function setUserMoney($money) {
        $this->userMoney = $money;
    }
    function getCanAddPicAttribute() {
        if (!$this->updated)$this->updateStatus();
        return $this->userMoney > $this->nextPicPrice;
    }
    function getNextPicPriceAttribute() {
        if (!$this->updated)$this->updateStatus();
        return $this->nextPicPrice;
    }
    function getPicsCountAttribute() {
        if (!$this->updated)$this->updateStatus();
        return $this->picsCount;
    }
    function getReportsAttribute() {
        if (!$this->updated)$this->updateStatus();
        return $this->reportsCollected;
    }
    function getPriceAttribute() {
        if (!$this->updated)$this->updateStatus();
        return $this->priceCollected;
    }
    function getBonesAttribute() {
        if (!$this->updated)$this->updateStatus();
        return $this->bonesCollected;
    }
    function getCanDeleteAttribute() {
        if (!$this->updated)$this->updateStatus();
        return $this->canDelete;
    }

    /**
     * Valid Species
     */
    function setSpecies($specie) {
        switch ($specie) {
            case 'desconocido':
            case 'perro':
            case 'gato':
            case 'roedor':
            case 'reptil':
            case 'ave':
            case 'equino':
            case 'granja':
            case 'exotico':
                $this->species = $specie;
                return true;
            
            default:
                $this->species = 'desconocido';
                return false;
        }
    }

    function setBirth($birth) {
        $test_arr  = explode('-', $birth);
        $year = (int)$test_arr[0];
        $month = (int)$test_arr[1];
        $day = (int)substr($test_arr[2], 0, 2);
        if (checkdate($month, $day, $year)) {
            $this->birth = $birth;
            return true;
        }
        return false;
        
    }

    function setSex($sex) {
        switch ($sex) {
            case '0':
            case '1':
            case '2':
                $this->sex = $sex;
                return true;
            default:
                $this->sex = '0';
                return false;

        }

    }


    function setDescription($description) {
        if ($description) {
            function unichr($i) {
                return iconv('UCS-4LE', 'UTF-8', pack('V', $i));
            }
            
            if (!preg_match('/^[a-zA-Z áéíóúAÉÍÓÚÑñ0-9]|['.
            unichr(0x1F300).'-'.unichr(0x1F5FF).
            unichr(0xE000).'-'.unichr(0xF8FF).
            ']\/u{0,50}$/', $description)) {
              return false;
            }
            $this->description = $description;
        } else {
            $this->description = '';
        }
        return true;

    }

    function setName($name) {
        if ($name) {
            $name = trim($name);
            if (!preg_match('/^[A-Za-zñÑáéíóúÁÉÍÓÚ ]{3,50}$/', $name)) {
              return false;
            }
        } else {
            return false;
        }
        $this->name = $name;
        return true;

    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at','updated_at','deleted_at', 'user_id'
    ];

    public function user()
    {
      return $this->belongsTo(User::class);
    }

    public function pics()
    {
        return $this->hasMany(Pic::class);
    }



}
