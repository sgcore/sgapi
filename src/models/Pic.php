<?php

namespace models;

use Illuminate\Database\Eloquent\Model;
class Pic extends Model
{
  protected $appends = [];

  protected $fillable = [
      'name', 'description', 'pet_id', 'user_id', 'deck_id', 'petname',
  ];
  protected $hidden = [
    'created_at','updated_at','deleted_at'
  ];

  public function pet()
  {
    return $this->belongsTo(Pet::class);
  }
  public function deck()
  {
    return $this->belongsTo(Deck::class);
  }
  public function user()
  {
    return $this->belongsTo(User::class);
  }

  public function image() {
    return new Image();
  }



}
