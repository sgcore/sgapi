<?php
namespace models;
use Illuminate\Database\Eloquent\Model;
class Sale extends Model
{
    protected $fillable = [
        'total', 'details', 'code',
    ];
}
