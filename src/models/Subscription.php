<?php
namespace models;
use Illuminate\Database\Eloquent\Model;
class Subscription extends Model
{
  public function negocio()
  {
      return $this->belongsTo('models\Negocio', 'negocio_id','Id');
  }
  public function usuario()
  {
      return $this->belongsTo('models\User');
  }
}
