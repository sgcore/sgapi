<?php

namespace models;

use Illuminate\Database\Eloquent\Model;
use clientes\Cliente;
use models\Product;
class User extends Model
{
    use DynamicHiddenVisible;
  protected $appends = ['CanAddNegocio', 'CanSincronize', 'TimeToExpire', 'ValidateOwnerCliente'];

    private $validClient = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'created_at',
        'updated_at',
        
    ];
    public static function showClientUser()
    {
        self::setStaticVisible(['id', 'name', 'email','status', 'role']);
    }
    public static function isValidClient($name){
        return !empty($name)
        && ctype_alnum($name);
    }
    public function getCanAddNegocioAttribute()
    {
      return $this->TimeToExpire > 0 && count($this->negocios) < 2;
    }
    public function getCanSincronizeAttribute()
    {
      return $this->TimeToExpire > 0;
    }
    public function getTimeToExpireAttribute()
    {
      $time = 0;
      if ($this->expire) {
        $datetime1 = new \DateTime();
        $datetime2 = new \DateTime($this->expire);
        $interval = $datetime2->diff($datetime1);
        $time = $interval->h;
      }
      return $time;
    }
    public function addTime($days){
        $today = date("Y-m-d H:i:s");
        $this->expire=date('Y-m-d', strtotime($today. ' + '.$days.' days'));;
        $this->save();

    }
    public function negocios(){
        return $this->hasMany('models\Negocio');
    }
    public function pcs(){
        return $this->hasMany('models\Pc');
    }
    public function subscriptions(){
        return $this->hasMany('models\Subscription');
    }
    public function Subscriptiones(){
      $this->load('subscriptions.negocio');
      return $this->subscriptions->map(function ($sub) {
          return $sub->negocio;
      });
    }
    public function Mynegocio($negid){
      foreach ($this->negocios as $n) {
        if($n->id === $negid){
          return $n;
        }
      }
      foreach ($this->subscriptions as $n) {
        if($n->id === $negid){
          return $n;
        }
      }
      return null;
    }
    public function getValidateOwnerClienteAttribute($cliente){
       $this->validClient = !empty($this->name) && $this->name == $cliente;
       return $this->validClient;
    }

    public function pathToOriginalImage($name = '')
    {
        return Cliente::pathClientFolder($this->name,'images/original/').$name;
    }
    public function changeImageProduct($producto, $new)
    {
        $iname = $producto->image;
        if($iname){
            $old = new Image($iname,false,$this->name);
            $old->deleteMe();
        }
        
        if($new->isValid()){
            $producto->image = $new->name();
            $producto->save();
            return true;
        }
        return false;
       
    }

    public function getProducto($codigo, $create = false, $trash = false)
    {
        $producto = null;
        $cliente = $this->name;
        if($this->validClient)
        {
            if($codigo)
            {
                $q = Product::on($cliente)->where(['Codigo' =>  $codigo]);
                if($trash){
                    //$q = $q->withTrashed();
                }
                $producto =$q->first();
            }
            if(!$producto && $create){
                $producto = Product::on($cliente)->create();
                $producto->Codigo = $codigo;
            }     
        }
        

        return $producto;
    }

    function RandomToken(){
        $this->access_token = bin2hex(random_bytes(16));
    }
    
    function ResetToken(){
        $this->RandomToken();
        $this->save();
    }
    static function loginWhitToken($token){
        return User::where('access_token','=',$token)->first();
      
    }

    function pets(){
        return $this->hasMany(Pet::class);
    }
    function decks(){
        return $this->hasMany(Deck::class);
    }
    function calculateNextPetPrice($petsCount, $picsCount)
    {
        $nextPetPrice = 100 * $petsCount - $picsCount;
        $nextPetPrice = $nextPetPrice < 100 && $petsCount > 0 ? 100 : $nextPetPrice;
        return $nextPetPrice;
    }
    function isMyOwnPic($pic)
    {
        return $this->id == $pic->user_id && ($pic->deck_id == 0 || $this->decks->search(function($item) use($id){
            return $item->id == $pic->deck_id;
        }));
    }
    function updateBones()
    {
        if($this->bones < 20)
        {
            $time = 0;
            $datetime1 = new \DateTime();
            if ($this->expire != null) {
               
                $datetime2 = new \DateTime($this->expire);
                $interval = $datetime2->diff($datetime1);
                $time = $interval->days;
            }
            if($time >= 1)
            {
                $this->bones = 20;
                $this->expire = new \DateTime();
                return true;
            }
        }
       
        return false;
    }
    function petsResponse() {
        $pets = $this->pets;
        $petsCount = 0;
        $picsCount = 0;
        foreach ($pets as $p) {
            $petsCount += 1;
            $picsCount += count($p->pics);
            $p->setUserMoney($this->coins);
        }
        $nextPetPrice = $this->calculateNextPetPrice($petsCount, $picsCount);
        return [
            'pets' => $this->pets,
            'nextPetPrice' => $nextPetPrice,
            'petsCount' => $petsCount,
            'cardsCount' => $picsCount,
            'coins' => $this->coins,
            'bones' => $this->bones,
            'status' => $this->status,
            'medals' => $this->medals,
            'level' => $this->level,
        ];
    }

}
