<?php
namespace models;
use Illuminate\Database\Eloquent\Model;
class Pc extends Model
{
  protected $fillable = [
      'key', 'user_id',
  ];
  public function User()
  {
      return $this->belongsTo('models\User');
  }
}
