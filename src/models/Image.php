<?php

namespace models;
use clientes\Cliente;


class Image
{
  private $name;
  private $size;
  private $path;
  private $isCached;
  private $cliente;
  private $server = './images/';
  private $original = 'original/';
  private $cached = 'cached/';
  private $sizes = [
    'icon' => [32, 32],
    'smallw' => [50, false],
    'smallh' => [false, 50],
    'mediumw' => [128, false],
    'mediumh' => [false, 128],
    'bigw' => [300, false],
    'bigh' => [false, 300],
    'giantw' => [640, false],
    'gianth' => [false, 640]
  ];

  public function __construct($name, $size, $client = '')
  {
    if(Cliente::validClient($client)){
      $this->server = Cliente::pathClientFolder($client, 'images/');
      $this->original = Cliente::pathClientFolder($client,'images/original/');
      $this->cached = Cliente::pathClientFolder($client,'images/cached/');
      $this->cliente = $client;
    }
   
    $original = $this->original.$name;
    $this->path = $original;
    $val1 = preg_match('/^[a-z.A-Z0-9]+$/', $name);
    $val2 = file_exists($this->path);

    if ( //original file exist
      $name
      && preg_match('/^[a-z.A-Z0-9]+$/', $name)
      && file_exists($this->path)
      && is_file($this->path)
    ) {
      $this->name = $name;
    } else {
      $original  = Cliente::noPicImagePath($client);
      $this->path =  $original;
      $this->name = "nopic.jpg";

    }
    if (// size is valid
      $size
      && array_key_exists ($size,$this->sizes)
    ) {
      $this->path = $this->cached.$size.$this->name;
      if ( // cached exist
        file_exists($this->path)
        && is_file($this->path)
      ) {
        $this->isCached = true;
      } else {//create cached
        $z = $this->sizes[$size];
        $this->path = $this->gd_resize($original, $this->path,$z[0], $z[1]);
      }
    }
  }
  public function originalPath() {
    return $this->original.$this->name;
  }
  public function path() {
    return $this->path ;
  }
  public function name() {

    return $this->name;
  }
  public function isValid()
  {
    return file_exists($this->originalPath()) && is_file($this->originalPath()) && file_exists($this->path) && is_file($this->path);
  }
  public function saveMe($imgData){
    if($cliente){
      $imgData->move($this->originalPath());
      
    } else {
      throw "no se establecio el negocio propietario";
    }
    
  }
  public function deleteMe()
  {
    if($this->isValid()){
      $valid = $this->cleanCache();
      unlink($this->originalPath());
      return $valid;
    }
    return false;
  }
  public function cleanCache()
  {
    if($this->isValid()){
      foreach($this->sizes as $k=>$v)
      {
        $file = $this->cached.$k.$this->name;
        if ( // cached exist
          file_exists($file)
          && is_file($file)
        ) {
          unlink($file);
        }
      }
      return true;
    }
    return false;
  }

  function gd_resize($image_path, $name, $rwidth=false, $rheight=false){

    list($width, $height) = getimagesize($image_path);

    if( !$rwidth ) $rwidth = $rheight * $width / $height;
    if( !$rheight ) $rheight = $rwidth * $height / $width;

    if( $rwidth > $width && $rheight > $height){
      return $image_path;
    }
    else if( $rwidth > $width ) $rwidth = $width;
    else if( $rheight > $height ) $rheight = $height;


    $im = imagecreatetruecolor($rwidth, $rheight);
    $src = imagecreatefromstring(file_get_contents($image_path));
    imagecopyresampled($im, $src, 0, 0, 0, 0, $rwidth, $rheight, $width, $height);

    imagejpeg($im, $name, 100);
    imagedestroy($im);
    imagedestroy($src);

    return $name;
  }



}
