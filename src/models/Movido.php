<?php
namespace models;
use Illuminate\Database\Eloquent\Model;
class Movido extends Model
{
    protected $table = 'gcobjetosmovidos';
    protected $visible = ['Producto', 'Cantidad', 'Monto'];
    public function Producto()
    {
      return $this->belongsTo('models\Product','Objeto', 'Id');
    }


}
