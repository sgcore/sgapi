<?php
namespace controllers;

use Silex\Application;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use models\Office;
class ClientOfficeController extends  ClientController
{
  public function connect(Application $app)
  {
      $controllers =parent::connect($app);
      // midleware before
      
      //users
      $controllers->options('/login', function (Request $request, Application $app){
        return $app->json([]);
      });
      
      $controllers->post('/all', function (Request $request, Application $app){
        $user = $app['user'];
        if (!$user || $user->role != "Admin") {
          return $app->json('No tiene autorizacion',401);
        }

        $cliente = $request->attributes->get("client");
        $filter = $request->request->get("filter");
        $per_page = $request->query->get("perPage") ?: 10;
        $page = $request->query->get("page") ?: 1;
        $q = Office::on($cliente);
        if($filter)
        {
          $arr =  explode(" ", $filter);
         //print all the value which are in the array
          foreach($arr as $v){
              $q->orWhere('name', 'like', "%{$v}%");
          }
          
        }


        $office = $q->paginate($per_page, ['id', 'name','address','phone', 'status', 'description', 'code'], 'page', $page);
        return $app->json($office); 
       
      });
      $controllers->post('/save', function (Request $request, Application $app){
        $user = $app['user'];
        if (!$user || $user->role != "Admin") {
          return $app->json('No tiene autorizacion',401);
        }
        $cliente = $request->attributes->get("client");

        //att
        $id = (int)$request->request->get('id');
        $address = $request->request->get('address');
        $phone = $request->request->get('phone');
        $name = $request->request->get('name');
        $code = $request->request->get('code');
        $status = $request->request->get('status');
        $description = $request->request->get('description');
        $isUpdate = $id > 0;
        $office = null;
        if($code)
        {
          $office = Office::on($cliente)->firstOrNew(['code' =>  $code]);
          if($office->wasRecentlyCreated) {
            if($id > 0)return $app->json('no puede asignar el id a una nueva sucursal',400);
            $office->status = "Creado";
          }else {
            if($id != $office->id)
            {
              return $app->json('La sucursal que quiere crear no le corresponde el codigo',400);
            }
          }
        } else {
          if($id > 0) {
            $office = Office::on($cliente)->where('id', $id)->first();
          } else {
            return $app->json('No puede crear una sucursal sin codigo',400);
          }

        }
        if($office){
          if($address)$office->address = $address;
          if($phone)$office->phone = $phone;
          if($name)$office->name = $name;
          if($status)$office->status = $status;
          if($description)$office->description = $description;
          if($office->save())
          {
            return $app->json($office); 
          }
        }
        
        return $app->json('no se pudo crear la sucursal',400);
        
       
      });
      $controllers->post('/delete/{id}', function (Request $request, Application $app){
        $user = $app['user'];
        if (!$user || $user->role != "Admin") {
          return $app->json('No tiene autorizacion',401);
        }

        $cliente = $request->attributes->get("client");
        $id = (int)$request->attributes->get("id");
        if($id > 0)
        {
          $other = Office::on($cliente)->find($id);
          if($other && $other->delete())
          {
            return $app->json($other); 
          }
        }
        return $app->json('No se encontro la sucursal',400);
       
      });
      return $controllers;
  }

}
