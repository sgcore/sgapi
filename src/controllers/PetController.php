<?php

namespace controllers;

use Silex\Application;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Silex\Api\ControllerProviderInterface;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Firebase\Auth\Token\Exception\InvalidToken;
use models\Image;
use models\User;
use models\Pet;
use models\Pic;
use clientes\Cliente;
class PetController implements ControllerProviderInterface
{
  public function connect(Application $app)
    {
      $controllers = $app['controllers_factory'];
      $controllers->post('/login', function(Request $request, Application $app){
        $idTokenString = $request->request->get('idtoken');
        $fbid = $request->request->get('fbid');
        $mcuser = null;

        if($fbid)
        { //facebook
          $fb = new \Facebook\Facebook([
            'app_id' => '620291354810783',
            'app_secret' => 'f9d213c03323c3286cd1bb567061af25',
            'default_graph_version' => 'v2.10',
            'default_access_token' => $idTokenString, // optional
          ]);
          
          // Use one of the helper classes to get a Facebook\Authentication\AccessToken entity.
          //   $helper = $fb->getRedirectLoginHelper();
          //   $helper = $fb->getJavaScriptHelper();
          //   $helper = $fb->getCanvasHelper();
          //   $helper = $fb->getPageTabHelper();
          
          try {
            // Get the \Facebook\GraphNodes\GraphUser object for the current user.
            // If you provided a 'default_access_token', the '{access-token}' is optional.
            $me = $fb->get('/me?fields=email')->getGraphUser();
            $fbid = $me->getId();
            $mail = $me->getEmail();
            if($mail)
            {
              $mcuser = User::firstOrCreate(['email' => $mail ]);
            } else {
              $mcuser = User::firstOrCreate(['fbid' =>  $fbid]);
            }

            
            if ($mcuser->wasRecentlyCreated) {
              $mcuser->fbid = $fbid;
              $mcuser->email = $mail;
              $mcuser->bones = 20;
            }
          } catch(\Facebook\Exceptions\FacebookResponseException $e) {
            // When Graph returns an error
            return $app->json(['message' => $e->getMessage()],401);
          } catch(\Facebook\Exceptions\FacebookSDKException $e) {
            // When validation fails or other local issues
            return $app->json(['message' => $e->getMessage()],401);
          }
          catch(Exception $e) {
            // When validation fails or other local issues
            return $app->json(['message' => $e->getMessage()],401);
          }
          
         

        } else { //firebase
          $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/google-services.json');
          $firebase = (new Factory)
              ->withServiceAccount($serviceAccount)
              ->create();
          
          try {
              $verifiedIdToken = $firebase->getAuth()->verifyIdToken($idTokenString);
              $uid = $verifiedIdToken->getClaim('sub');
              $user = $firebase->getAuth()->getUser($uid);
              $mcuser = User::firstOrCreate(['email' =>  $user->email]);
              if ($mcuser->wasRecentlyCreated) {
                $mcuser->fbid = '';
                $mcuser->bones = 20;
              }
          } catch (InvalidToken $e) {
              return $app->json(['message' => $e->getMessage()],401);
          }
        }
        
        if($mcuser == null)
        {
          return $app->json(['message' => 'Se produjo un error al crear el usuario'],401);
        }
        $mcuser->updateBones(); //amtes de resettoken para que se guarde
        $mcuser->ResetToken();
        $ret = $mcuser->petsResponse() + ['token' => $mcuser->access_token];
        return $app->json($ret);

      });
      $controllers->get('/cards', function(Request $request, Application $app){
        $count = (int)$request->query->get('count');
        if(!$count)
        {
          $count = 10;
        }
        $cards= Pic::inRandomOrder()->take($count)->get();
        return $app->json($cards);
      });

      $controllers->put('/card/{petid}', function(Request $request, Application $app){
        $user = $app['user'];
        if (!$user) {
          return $app->json('No tiene autorizacion',401);
        }

        $petid = (int)$request->attributes->get('petid');
        $user->load('pets');
        $pets = $user->pets;
        $pet = null;
        foreach ($pets as $p) {
          if ($petid == $p->id){
            $pet = $p;
            break;
          }

        }
        if (!$pet) {
          return $app->json('La mascota no le pertenece',400);
        }
        if ($pet->nextPicPrice > $user->coins) {
          return $app->json('te faltan '.($pet->nextPicPrice - $user->coins).' moneditas',400);
        }

        $image =  $request->files->get('image');
        $name = md5($user->email.uniqid()).'.jpg';
        if ($image) {
          if ($image->isValid()) {
            try {
              $image->move(Cliente::pathClientFolder('pets','images/original/'), $name);
              $description =  $request->request->get('description');
              $pic = new Pic([
                'name' => $name,
                'description' => $description,
                'pet_id' => (int)$petid,
                'petname' => $pet->name,
                'user_id' => $user->id
              ]);
              //cobramos
              $user->coins -= $pet->nextPicPrice;
              $pet->setUserMoney($user->coins);


              if($pic->save() && $user->save() ) {
                $pet->pics->push($pic);
                return  $app->json($pet->updateStatus());
              }
            } catch (Exception $e) {
              return $app->json('Se produjo un error al intentar guardar',400);
            }

          }
        }
        return $app->json('Se produjo un error',400);

      });
      $controllers->post('/petcard/{petid}', function(Request $request, Application $app){
        $user = $app['user'];
        if (!$user) {
          return $app->json('No tiene autorizacion',401);
        }
        $petid = (int)$request->attributes->get('petid');
        $user->load('pets');
        $pets = $user->pets;
        $pet = null;
        foreach ($pets as $p) {
          if ($petid == $p->id){
            $pet = $p;
            break;
          }

        }
        if (!$pet) {
          return $app->json('La mascota no le pertenece',400);
        }
        if ($pet->nextPicPrice > $user->coins) {
          return $app->json('te faltan '.($pet->nextPicPrice - $user->coins).' moneditas',400);
        }
        $description = $request->request->get('description');
        $img = $request->request->get('ImageBytes');
        $img = str_replace('data:image/png;base64,', '', $img);
        $img = str_replace(' ', '+', $img);
        $data = base64_decode($img);
        $name = md5($user->email.uniqid()).'.jpg';
        $file = Cliente::pathClientFolder('pets','images/original/'). $name;
        $success = file_put_contents($file, $data);
        if ($success ) {
          try {
            $pic = new Pic([
              'name' => $name,
              'description' => $description,
              'pet_id' => (int)$petid,
              'petname' => $pet->name,
              'user_id' => $user->id
            ]);
            //cobramos
            $user->coins -= $pet->nextPicPrice;
            $pet->setUserMoney($user->coins);


            if($pic->save() && $user->save() ) {
              $pet->pics->push($pic);
              return  $app->json($pet->updateStatus());
            }
          } catch (Exception $e) {
            return $app->json('Se produjo un error al intentar guardar',400);
          }
        }
        return $app->json('Se produjo un error',400);

      });
      $controllers->post('/card/{id}', function(Request $request, Application $app){
        $user = $app['user'];
        if (!$user) {
          return $app->json('No tiene autorizacion',401);
        }
        $id = (int)$request->attributes->get('id');
        $pic = Pic::find($id);
        if (!$pic || !$user->isMyOwnPic($pic)) {
          return $app->json('La tarjeta no le pertenece',400);
        }
        if ($pic->deleted_at) {
          return $app->json('La tarjeta ya fue eliminada',400);
        }
        
        $pic->description = $request->request->get('description');
        if($pic->save())
        {
          return $app->json('Se actualizo la tarjeta');
        }


        return $app->json('Se produjo un error',400);

      });
      $controllers->post('/bones', function(Request $request, Application $app){
        $user = $app['user'];
        $bones = 0;
        $bonesConsumed = 0;
        $coins = 0;
        $coinsGained = 0;

        $ids = $request->request->get('cards');

        $counts = count($ids);
        if($user && $counts > 0 && $user->bones >= $counts)
        {
          $q = Pic::whereIn('id', $ids);
          $all = $q->get();
          $boned = $q->increment('bones');
          if($boned > 0)
          {
            $user->bones = $user->bones - $boned;
            $acoins = $all->sum(function ($card) {
                $rounded = round($card->bones / 100);
                return rand(0, 100 + $rounded) > 50 ? 1 + $rounded : 0;
            });
            $user->coins = $user->coins + $acoins;
            if($user->save())
            { 
              $bones = $user->bones;
              $bonesConsumed = $boned;
              $coins = $user->coins;
              $coinsGained = $acoins;
              
            } else {
              return $app->json('se produjo un error inseperado',400);
            }
          }
        }       


        $ret = [
          'coins' => $coins,
          'bones' => $bones,
          'coinsGained' => $coinsGained ,
          'bonesConsumed' => $bonesConsumed,
          'deck' => Pic::inRandomOrder()->take(20)->get()
        ];
        return $app->json($ret);

      });
      $controllers->delete('/card/{id}', function(Request $request, Application $app){
        $user = $app['user'];
        if (!$user) {
          return $app->json('No tiene autorizacion',401);
        }
        $id = (int)$request->attributes->get('id');
        $user->load('pets.pics');
        $pets = $user->pets;
        $pic = null;
        $petindex = false;
        $pet = null;
        for($i = 0; $i < count($pets); $i++) {
          $pet = $user->pets[$i];
          $index = $pet->pics->search(function($item) use($id){
              return $item->id == $id;
          });
          if($index !== false)
          {
            $pic = $pet->pics[$index];
            $petindex = $i;
            break;
          }
        }

        if (!$pic) {
          return $app->json('La tarjeta no le pertenece',400);
        }
        if ($pic->deleted_at) {
          return $app->json('La tarjeta ya fue eliminada',400);
        }
        $img = new Image($pic->name, false, "pets");
        $img->deleteMe();

        if (Pic::destroy($id)) {
          return $app->json("Se elimino la tarjeta");
        }

      });
      $controllers->post('/', function(Request $request, Application $app){
          $user = $app['user'];
          if (!$user) {
            return $app->json('No tiene autorizacion',401);
          }
          
          $pet = new Pet();

          # validate name
          $name = $request->request->get('name');
          if (!$pet->setName($name)) {
            return $app->json('El nombre debe tener entre 3 y 50 letras (solo letras)',400);
          }


          # validate description
          $description = $request->request->get('description');
          if (!$pet->setDescription($description)) {
            return $app->json('La descripcion debe tener menos de 50 letras o números',400);
          }

          # validate sex
          $sex = $request->request->get('sex');
          $pet->setSex($sex);

          # validate specie
          $specie = $request->request->get('species');
          $pet->setSpecies($specie);
          
          # validate birth
          $birth = $request->request->get('birth');
          $pet->setBirth($birth);
          
          
          $user->load('pets');
          $pets = $user->pets;
          foreach ($pets as $p) {
            if (preg_match('/('.$p->name.')/', $name)){
              return $app->json('Ya tiene una mascota con ese nombre',400);
            }

          }
          $user->updateBones(); 
          $ret = $user->petsResponse();

          // calculamos
          if($ret['nextPetPrice'] > $user->coins)
          {
            return $app->json('No tiene suficientes moneditas',400);
          }
          //cobramos
          $user->coins -= $ret['nextPetPrice'];

          //actualizamos el precio y los coins
          $ret['nextPetPrice'] = $user->calculateNextPetPrice($ret['petsCount'] + 1, $ret['cardsCount']);
          $ret['coins'] = $user->coins;

          $pet->user_id = $user->id;

          
           
          if ($pet->save() && $user->save()) {
           
            $ret['pets'][] = $pet;
            return $app->json($ret);
          }
          return $app->json('Se produjo un error',400);

      });
      $controllers->post('/{id}', function(Request $request, Application $app){
        $user = $app['user'];
        if (!$user) {
          return $app->json('No tiene autorizacion',401);
        }
        $id = (int)$request->attributes->get('id');
        
        $pet = null;

        # validate name
        $name = $request->request->get('name');
        $user->load('pets');
        $pets = $user->pets;
        foreach ($pets as $p) {
          if ($p->id != $id) {
            if (preg_match('/('.$p->name.')/', $name)){
               return $app->json('Ya tiene una mascota con ese nombre',400);
            }
          } else {
            $pet = $p;
          }
        }

        if(!$pet) {
          return $app->json('No se encontro la mascota',400);
        }



        if (!$pet->setName($name)) {
          return $app->json('El nombre debe tener entre 3 y 30 letras (solo letras)',400);
        }


        # validate description
        $description = $request->request->get('description');
        if (!$pet->setDescription($description)) {
          return $app->json('La descripcion debe tener menos de 30 letras o números',400);
        }

        # validate sex
        $sex = $request->request->get('sex');
        $pet->setSex($sex);

        # validate specie
        $specie = $request->request->get('species');
        $pet->setSpecies($specie);
        
        # validate birth
        $birth = $request->request->get('birth');
        $pet->setBirth($birth);

        //actualizamos las tarjetas
        $count = count($pet->pics);
        for($i = 0; $i < $count; $i++)
        {
          if($user->isMyOwnPic($pet->pics[$i]))
          {
            $pet->pics[$i]->petname = $pet->name;
            $pet->pics[$i]->save();
          }
          
        }
        $pet->user_id = $user->id;
        if ($pet->save()) {
          return $app->json($user->petsResponse());
        }
        return $app->json('Se produjo un error',400);

      }); 
      $controllers->delete('/{id}', function(Request $request, Application $app){
        $user = $app['user'];
        if (!$user) {
          return $app->json('No tiene autorizacion',401);
        }
        $id = (int)$request->attributes->get('id');
        
        $user->load('pets');
        $pets = $user->pets;
        //buscamos la pet
        $index = $pets->search(function($item) use($id){
            return $item->id == $id;
        });
        
        if($index === false) {
          return $app->json('No se encontro la mascota',400);
        }

        $pet =$pets[$index];

        
        $pet->load("pics");
        $pics = $pet->pics;
        if(count($pics) > 0)
        {
          return $app->json('No puedes eliminar una mascota que tenga tarjetas',400);
        }

        
        if (Pet::destroy($id)) {
          return $app->json('Se elimino la mascota');
        }
        
        return $app->json('Se produjo un error',400);

      });
      $controllers->get('/', function(Request $request, Application $app){
        $user = $app['user'];
        if (!$user) {
          return $app->json('No tiene autorizacion',401);
        }
        $user->load('pets.pics');
        
        return $app->json($user->petsResponse());

      });
      
      // Imagen original
      $controllers->get('/images/{image}', function(Request $request, Application $app){
        $name = $request->attributes->get('image');
        $porciones = explode("_", $name);
        $size = false;
        if(is_array($porciones) && count($porciones) == 2){
          $size = $porciones[0];
          $name = $porciones[1];
        }
        $img = new Image($name, $size, "pets"); //cliente pets
        header("Content-Type: image/jpg");
        header('Cache-Control: private, max-age=14400');
        header('Expires: ' . gmdate('D, d M Y H:i:s', time() + 14400) . ' GMT');
        $r = file_get_contents($img->path());
        header('Etag: ' . md5($r));

        die($r);

      });
      
     
      return $controllers;
    }

}
