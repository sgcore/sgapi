<?php
namespace controllers;

use Silex\Application;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use models\Item;
use models\Tag;
use clientes\Cliente;
class ClientTagController extends  ClientController
{
  public function connect(Application $app)
  {
      $controllers = parent::connect($app);
      // midleware before
      
      $controllers->post('/all', function (Request $request, Application $app){
       
        $cliente = $request->attributes->get("client");
        $filter = $request->request->get("filter");
        $per_page = $request->query->get("perPage") ?: 10;
        $page = $request->query->get("page") ?: 1;
        $q = Tag::on($cliente);
        if($filter)
        {
          $arr =  explode(" ", $filter);
          //print all the value which are in the array
          foreach($arr as $v){
              $q->orWhere('tag', 'like', "%{$v}%");
          }
          
        }


        $tag = $q->paginate($per_page, ['id', 'tag', 'description', 'private'], 'page', $page);
        return $app->json($tag); 
       
      });

      $controllers->post('/save', function (Request $request, Application $app){
        $user = $app['user'];
        if (!$user || $user->role != "Admin") {
          return $app->json('No tiene autorizacion',401);
        }
        $cliente = $request->attributes->get("client");

        //att
        $id = (int)$request->request->get('id');
        $tag = $request->request->get('tag');
        $description = $request->request->get('description');
        $private = $request->request->get('private');
        $isUpdate = $id > 0;
        $product = null;
        if($tag)
        {
          $product = Tag::on($cliente)->where(['tag' =>  $tag])->first();
          if($product && $id !== $product->id)
          {
            return $app->json('Parece que la etiqueta ya existe.',400);
          }
          if($isUpdate)
          {
            $product = Tag::on($cliente)->where('id', $id)->first();
            $product->tag = $tag;
          } else {
            $product = Tag::on($cliente)->create(['tag' =>  $tag]);
          }

        } else {
          if($isUpdate) {
            $product = Tag::on($cliente)->where('id', $id)->first();
          } else {
            return $app->json('No puede crear un tag sin nombre',400);
          }

        }
        if($product){
           if($description)$product->description = $description;
           if($private != null)$product->private = json_decode($private);
          if($product->save())
          {
            return $app->json($product); 
          }
        }
        
        return $app->json('no se pudo crear la etiqueta',400);
        
       
      });
      $controllers->post('/delete/{id}', function (Request $request, Application $app){
        $user = $app['user'];
        if (!$user || $user->role != "Admin") {
          return $app->json('No tiene autorizacion',401);
        }

        $cliente = $request->attributes->get("client");
        $id = (int)$request->attributes->get("id");
        if($id > 0)
        {
          $other = Tag::on($cliente)->find($id);
          if($other && $other->delete())
          {
            return $app->json($other); 
          }
        }
        return $app->json('No se encontro el producto',400);
       
      });

      return $controllers;
  }

}
