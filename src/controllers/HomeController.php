<?php
namespace controllers;

use Silex\Application;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Silex\Api\ControllerProviderInterface;
class HomeController implements ControllerProviderInterface
{
  public function connect(Application $app)
  {
      $controllers = $app['controllers_factory'];
     
      $controllers->get('/', function (Request $request, Application $app){
        $user = $app['user'];
        /*if($user){
          return $app->json(["message" => "hola mundo"]);
        }*/
        return $app->sendFile('./web/index.html');
      });
      return $controllers;
  }

}
