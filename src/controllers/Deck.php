<?php

namespace models;

use Illuminate\Database\Eloquent\Model;
class Deck extends Model
{
  protected $appends = [];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'user_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at','updated_at','deleted_at', 'user_id'
    ];

    public function user()
    {
      return $this->belongsTo(User::class);
    }
    public function pics()
    {
      return $this->hasMany(Pic::class);
    }


}
