<?php
namespace controllers;

use Silex\Application;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Silex\Api\ControllerProviderInterface;
use models\Product;
class ProductController implements ControllerProviderInterface
{
  public function connect(Application $app)
  {
      $controllers = $app['controllers_factory'];
      $controllers->get('/all', function (Request $request, Application $app){
        $state = $request->query->get('state');
        $carac = $request->query->get('showcara');
        $costo = $request->query->get('showcost');
        $codigo = $request->query->get('showcode');

        if($carac) {
          Product::showCaract();
        }
        if($costo) {
          Product::showCosto();
        }
        if($codigo) {
          Product::showCodigo();
        }
        Product::showColums(['Id', 'Nombre', 'Descripcion', 'Codigo', 'Precio','Decendents']);
        Product::showAll();
        $prods = Product::roots()->with('Decendents')->get();
        return $app->json($prods);
      });
      $controllers->get('/view', function (Request $request, Application $app){
        $codigo = $request->query->get('c');
        $id = $request->query->get('id');
        $prod = null;
        Product::showAll();
        if($id) {
          $prod = Product::findOrFail($id);
        } else if($codigo) {
          $prod = Product::where('codigo','=',$codigo)->get();
        }
        if($prod) {
            return $app->json($prod);
        }
        return $app->json('No se obtubo el producto', 401);
      });
      $controllers->get('/prodonly', function (Request $request, Application $app){
        $state = $request->query->get('state');
        $carac = $request->query->get('showcara');
        $costo = $request->query->get('showcost');
        $codigo = $request->query->get('showcode');

        if($carac) {
          Product::showCaract();
        }
        if($costo) {
          Product::showCosto();
        }
        if($codigo) {
          Product::showCodigo();
        }
        Product::showBasic();
        $prods = Product::on('dig')->products()->get();
        return $app->json($prods);
      });
      return $controllers;
  }

}
