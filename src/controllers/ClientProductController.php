<?php
namespace controllers;

use Silex\Application;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use models\Item;
use models\Imageitem;
use models\Image;
use clientes\Cliente;
class ClientProductController extends  ClientController
{
  public function connect(Application $app)
  {
      $controllers = parent::connect($app);
      // midleware before
      
      $controllers->post('/all', function (Request $request, Application $app){
        $user = $app['user'];
        if (!$user) {
          return $app->json('No tiene autorizacion',401);
        }

        $cliente = $request->attributes->get("client");
        $filter = $request->request->get("filter");
        $per_page = $request->query->get("perPage") ?: 10;
        $page = $request->query->get("page") ?: 1;
        $q = Item::on($cliente)->with("images")->with("tags");
        if($filter)
        {
          $q->orWhere('code', '=', $filter);
          $arr =  explode(" ", $filter);
          //print all the value which are in the array
          foreach($arr as $v){
              $q->orWhere('name', 'like', "%{$v}%");
          }
          
        }


        $product = $q->paginate($per_page, ['id', 'name','price', 'status', 'description', 'code', 'image'], 'page', $page);
        return $app->json($product); 
       
      });
      $controllers->post('/view', function (Request $request, Application $app){
        $user = $app['user'];
        $cliente = $request->attributes->get("client");
        $code = $request->request->get('code');
        if(!$code)
        {
          return $app->json('no se ingreso el codigo del producto',400);
        }

        $product = Item::on($cliente)->where(['code' =>  $code])->first();
        if(!$product)
        {
          return $app->json('no se encontro el producto',400);
        }
        if ($user) {
          $product->load('images');
          $product->load('tags');
        }
        return  $app->json($product);
       
      });

      $controllers->post('/save', function (Request $request, Application $app){
        $user = $app['user'];
        if (!$user || $user->role != "Admin") {
          return $app->json('No tiene autorizacion',401);
        }
        $cliente = $request->attributes->get("client");

        //att
        $id = (int)$request->request->get('id');
        $price = $request->request->get('price');
        $name = $request->request->get('name');
        $code = $request->request->get('code');
        $status = $request->request->get('status');
        $description = $request->request->get('description');
        $details = json_decode($request->request->get('details'));
        



        $isUpdate = $id > 0;
        $product = null;
        if($code)
        {
          $product = Item::on($cliente)->firstOrNew(['code' =>  $code]);
          if($product->wasRecentlyCreated) {
            if($id > 0)return $app->json('no puede asignar el id a un nuevo producto',400);
            $product->status = "Creado";
          }else {
            if($id != $product->id)
            {
              return $app->json('El producto que quiere crear no le corresponde el codigo',400);
            }
            $product->load('images');
          }
        } else {
          if($id > 0) {
            $product = Item::on($cliente)->where('id', $id)->first();
            $product->load('images');
          } else {
            return $app->json('No puede crear un producto sin codigo',400);
          }

        }
        if($product){
          if($price)$product->price = $price;
          if($name)$product->name = $name;
          if($status)$product->status = $status;
          if($description)$product->description = $description;
          if($details)$product->details = json_encode($details);

          //tags
          $tags = collect($details->tags)->map(function ($item, $key) {
            return $item->id;
          });
          $otags = $product->tags->map(function ($item, $key) {
            return $item->id;
          });
          
          if($tags->count() > 0)
          {
            $att = $tags->diff($otags);
            $product->tags()->attach($att);
          }
          if($otags->count() > 0)
          {
            $dea = $otags->diff($tags);
            $product->tags()->detach($dea);
          }
         

          if($product->save())
          {
            return $app->json($product); 
          }
        }
        
        return $app->json('no se pudo crear la producto',400);
        
       
      });
      $controllers->post('/delete/{id}', function (Request $request, Application $app){
        $user = $app['user'];
        if (!$user || $user->role != "Admin") {
          return $app->json('No tiene autorizacion',401);
        }

        $cliente = $request->attributes->get("client");
        $id = (int)$request->attributes->get("id");
        if($id > 0)
        {
          $other = Item::on($cliente)->find($id);
          if($other)
          {

            //images
            $other->loadMissing("images");
            $images = $other->images;
            $imgids = [];
            foreach ($images as $image) {
              $fisica = new Image($image->name, false, $cliente);
              $imgids[] = $image->id;
              $fisica->deleteMe();
            }
            //Imageitem::on($cliente)->destroy($imgids);
            $other->images()->delete($imgids);

            //tags
            $other->loadMissing("tags");
            $other->tags()->detach();

            if($other->delete())return $app->json($other); 
          }
        }
        return $app->json('No se encontro el producto',400);
       
      });

      $controllers->post('/image', function (Request $request, Application $app){
        $user = $app['user'];
        if (!$user || $user->role != "Admin") {
          return $app->json('No tiene autorizacion',401);
        }
        $cliente = $request->attributes->get("client");

        //att
        $code = $request->request->get('code');
        $image =  $request->files->get('image');
        if(!$image){
          $image =  $request->files->get('file');
        }
        $description =  $request->request->get('description');
        $product = null;
        if($code)
        {
          $product = Item::on($cliente)->where(['code' =>  $code])->first();
          if($product)
          {
            $product->load('images');
            $name = md5($code.uniqid()).'.jpg';
            if ($image) {
              if ($image->isValid()) {
                try {
                  $image->move(Cliente::pathClientFolder($cliente,'images/original/'), $name);
                  $pic = Imageitem::on($cliente)->create([
                    'name' => $name,
                    'description' => $description,
                    'item_id' => (int)$product->id,
                    'target' => 'item',
                  ]);
                  $product->image=$name;
                  if($product->save()) {
                    $product->images->push($pic);
                    return  $app->json($product);
                  }
                } catch (Exception $e) {
                  return $app->json('Se produjo un error al intentar guardar',400);
                }

              }
            }
          }
         
        } 
        return $app->json('no se pudo agregar la imagen del producto',400);
        
       
      });
      $controllers->post('/image/update/{id}', function (Request $request, Application $app){
        $user = $app['user'];
        if (!$user || $user->role != "Admin") {
          return $app->json('No tiene autorizacion',401);
        }
        $cliente = $request->attributes->get("client");
        $id = (int)$request->attributes->get("id");
        $description = $request->request->get('description');
        if($id)
        {
          $img = Imageitem::on($cliente)->find($id);
          
            
          if($img)
          {
            $img->description = $description;
            if($img->save())
            {
              return $app->json($img); 
            }
          }
        }
        
        
        return $app->json('no se pudo actualizar la imagen',400);
        
       
      });
      $controllers->post('/image/delete/{id}', function (Request $request, Application $app){
        $user = $app['user'];
        if (!$user || $user->role != "Admin") {
          return $app->json('No tiene autorizacion',401);
        }
        $cliente = $request->attributes->get("client");
        $id = (int)$request->attributes->get("id");
        if($id)
        {
          $img = Imageitem::on($cliente)->find($id);
          
            
          if($img)
          {
            $p = Item::on($cliente)->find($img->item_id);
            if($p)
            {
              $images = $p->images;
              if($images->count() > 1)
              {
                $p->image=$images[0]->name;
              } else {
                $p->image= "nopic.jpg" ;
              }
              $p->save();
            }

            
            
            
            if($img->delete())
            {
              $fisica = new Image($img->name, false, $cliente);
              $fisica->deleteMe();
              return $app->json($img); 
            }
          }
        }
        
        
        return $app->json('no se pudo eliminar la imagen',400);
        
       
      });
      return $controllers;
  }

}
