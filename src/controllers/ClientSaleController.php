<?php
namespace controllers;

use Silex\Application;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use models\Sale;
class ClientSaleController extends  ClientController
{
  public function connect(Application $app)
  {
      $controllers =parent::connect($app);
     
      $controllers->post('/all', function (Request $request, Application $app){
        $user = $app['user'];
        if (!$user) {
          return $app->json('No tiene autorizacion',401);
        }
        $onlyMy = $user->role != "Admin";


        $cliente = $request->attributes->get("client");
        $filter = $request->request->get("filter");
        $per_page = $request->query->get("perPage") ?: 10;
        $page = $request->query->get("page") ?: 1;
        $q = Sale::on($cliente);
        if($onlyMy)$q->where("user_id", $user->id);
        if($filter)
        {
          $arr =  explode(" ", $filter);
         //print all the value which are in the array
          foreach($arr as $v){
              $q->orWhere('details', 'like', "%{$v}%");
          }
          
        }


        $sales = $q->paginate($per_page, ['id', 'code','details', 'status', 'created_at', 'total'], 'page', $page);
        return $app->json($sales); 
       
      });
      $controllers->post('/view', function (Request $request, Application $app){
        $user = $app['user'];
        $cliente = $request->attributes->get("client");
        $code = $request->request->get('code');
        if(!$code)
        {
          return $app->json('no se ingreso el codigo del movimiento',400);
        }

        $sale = Sale::on($cliente)->where(['code' =>  $code])->first();
        if(!$sale)
        {
          return $app->json('no se encontro el movimiento',400);
        }
        return  $app->json($sale);
        
       
      });
      $controllers->post('/save', function (Request $request, Application $app){
        $user = $app['user'];
        if (!$user) {
          return $app->json('No tiene autorizacion',401);
        }
        $onlyMy = $user->role != "Admin";
        $cliente = $request->attributes->get("client");

        //att
        $details = $request->request->get('details');
        $id = (int)$request->request->get('id');
        $code = $request->request->get('code');
        $total = $request->request->get('total');
        $status = $request->request->get('status');
        $isUpdate = $id > 0;
        $sale = null;
        if($code)
        {
          $sale = Sale::on($cliente)->firstOrNew(['code' =>  $code]);
          if($sale->wasRecentlyCreated) {
            if($id > 0)return $app->json('no puede asignar el id a un movimiento nuevo',400);
            $sale->status = "Creado";
          }else {
            if($id != $sale->id)
            {
              return $app->json('El movimiento que quiere editar no le corresponde el codigo',400);
            }
          }
        } else {
          if($id > 0) {
            $sale = Sale::on($cliente)->where('id', $id)->first();
          } else {
            return $app->json('No puede crear un movimiento sin codigo',400);
          }

        }
        if($sale){
          if($onlyMy && $sale->user_id != $user->id)
          {
            return $app->json('El movimiento no le pertenece',400);
          }
          if($details)$sale->details = $details;
          if($status)$sale->status = $status;
          if($total)$sale->total = $total;
          if($sale->save())
          {
            return $app->json($sale); 
          }
        }
        
        return $app->json('no se pudo crear el movimiento',400);
        
       
      });
      $controllers->post('/delete/{id}', function (Request $request, Application $app){
        $user = $app['user'];
        if (!$user || $user->role != "Admin") {
          return $app->json('No tiene autorizacion',401);
        }

        $cliente = $request->attributes->get("client");
        $id = (int)$request->attributes->get("id");
        if($id > 0)
        {
         $other = Sale::on($cliente)->find($id);
          if($other && $other->delete())
          {
            return $app->json($other); 
          }
        }
        return $app->json('No se encontro el movimiento',400);
       
      });

      return $controllers;
  }

}
