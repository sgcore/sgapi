<?php
namespace controllers;

use Silex\Application;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use models\User;
class ClientUserController extends  ClientController
{
  public function connect(Application $app)
  {
      $controllers =parent::connect($app);
      // midleware before
      
      //users
      $controllers->options('/login', function (Request $request, Application $app){
        return $app->json([]);
      });
      $controllers->post('/login', function (Request $request, Application $app){
        $email = $request->request->get('email');
        $pass = $request->request->get('password');
        $cliente = $request->attributes->get("client");
        $user = User::on($cliente)->where('email', $email)->first();
        if($user && password_verify($pass, $user->password))
        {
          $user->access_token = bin2hex(random_bytes(16));
          $user->save();
          return $app->json($user); 
        }
        return $app->json('los datos no pertenecen a nadie conocido',401);
       
      });

      $controllers->post('/all', function (Request $request, Application $app){
        $user = $app['user'];
        if (!$user || $user->role != "Admin") {
          return $app->json('No tiene autorizacion',401);
        }

        $cliente = $request->attributes->get("client");
        $filter = $request->request->get("filter");
        $per_page = $request->query->get("perPage") ?: 10;
        $page = $request->query->get("page") ?: 1;
        $q = User::on($cliente);
        if($filter)
        {
          $arr =  explode(" ", $filter);
         //print all the value which are in the array
          foreach($arr as $v){
              $q->orWhere('name', 'like', "%{$v}%");
          }
          
        }


        $users = $q->paginate($per_page, ['id', 'name','email', 'status', 'role'], 'page', $page);
        return $app->json($users); 
       
      });
      $controllers->post('/save', function (Request $request, Application $app){
        $user = $app['user'];
        if (!$user || $user->role != "Admin") {
          return $app->json('No tiene autorizacion',401);
        }
        $cliente = $request->attributes->get("client");

        //att
        $email = $request->request->get('email');
        $id = (int)$request->request->get('id');
        $role = $request->request->get('role');
        $name = $request->request->get('name');
        $pass = $request->request->get('password');
        $status = $request->request->get('status');
        $isUpdate = $id > 0;
        $user = null;
        if($email)
        {
          $user = User::on($cliente)->firstOrNew(['email' =>  $email]);
          if($user->wasRecentlyCreated) {
            if($id > 0)return $app->json('no puede asignar el id a un usuario nuevo',400);
            $user->status = "Creado";
          }else {
            if($id != $user->id)
            {
              return $app->json('El usuario que quiere editar no le corresponde el email',400);
            }
          }
        } else {
          if($id > 0) {
            $user = User::on($cliente)->where('id', $id)->first();
          } else {
            return $app->json('No puede crear un usuario sin email',400);
          }

        }
        if($user){
          if($pass)$user->password = password_hash ($pass, PASSWORD_DEFAULT);
          if($name)$user->name = $name;
          if($role)$user->role = $role;
          if($status)$user->status = $status;
          if($user->save())
          {
            return $app->json($user); 
          }
        }
        
        return $app->json('no se pudo crear el usuario',400);
        
       
      });
      $controllers->post('/delete/{id}', function (Request $request, Application $app){
        $user = $app['user'];
        if (!$user || $user->role != "Admin") {
          return $app->json('No tiene autorizacion',401);
        }

        $cliente = $request->attributes->get("client");
        $id = (int)$request->attributes->get("id");
        if($id > 0)
        {
          if($id == $user->id)
          {
            return $app->json('No puede auto-eliminarse',400);
          }
          $other = User::on($cliente)->find($id);
          if($other && $other->delete())
          {
            return $app->json($other); 
          }
        }
        return $app->json('No se encontro el usuario',400);
       
      });
      return $controllers;
  }

}
