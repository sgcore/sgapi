<?php
namespace controllers;

use Silex\Application;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Silex\Api\ControllerProviderInterface;
use models\Movimiento;
class MovimientoController implements ControllerProviderInterface
{
  public function connect(Application $app)
    {
        $controllers = $app['controllers_factory'];
        $controllers->get('/all', function (Request $request, Application $app){
          $tipo = $request->query->get('t');
          $caja = $request->query->get('c');
          $q = Movimiento::with(['Pagos','Movidos.Producto']);
          if($tipo)
          {
            $q->where('Tipo','=',$tipo);
          }
          if($caja)
          {
            $q->where('Caja','=',$caja);
          }
          return $app->json($q->get());
        });
        return $controllers;
    }

}
