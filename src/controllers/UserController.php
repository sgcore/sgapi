<?php

namespace controllers;

use Silex\Application;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Silex\Api\ControllerProviderInterface;
use Illuminate\Support\Collection;
use Google_Client;
use models\User;
use models\Negocio;
use models\Pc;
class UserController implements ControllerProviderInterface
{
  public function connect(Application $app)
    {
        $controllers = $app['controllers_factory'];
        $controllers->post('/glogin', function(Request $request, Application $app){
          $id_token=$request->request->get('id_token');
          if(empty($id_token)) {
            return $app->json('No establecio el token',401);
          }
          $CLIENT_ID = '417176926357-n1i0r5c0lrsfk2uqeeb9jpqicmle4lh0.apps.googleusercontent.com';
          $client = new Google_Client(['client_id' => $CLIENT_ID]);
          $payload = $client->verifyIdToken($id_token);
          if ($payload) {
            $user = User::firstOrCreate(['email' =>  $payload['email']]);
            $user->fbid=$payload['sub'];
            $user->access_token=$id_token;
            $user->name=$payload['name'];
            $user->randomToken();
            $user->load('negocios');
            if($user->save()){
                $user['token']=$user->access_token
                        .'-'
                        .$user->fbid;
                $user['suscripciones']= $user->Subscriptiones();
                unset($user['subscriptions']);
                return $app->json($user);
            }
          } else {
            // Invalid ID token
          }
          return $app->json('No tiene autorizacion',401);

        });

        $controllers->post('/login', function(Request $request, Application $app){
          $email = $request->request->get('email');
          $pass = $request->request->get('pass');
          $key = $request->request->get('key');
          list($pckey, $pcname) = explode("@", $key);

          if ($pass && $pckey && filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $user = User::firstOrCreate(['email' =>  $email]);
            $pc = Pc::firstOrCreate(['key' => $pckey]); //key
            $fecha = new \DateTime(date("Y-m-d H:i:s")); //now
            
/*
            if($pc->user_id && $pc->user_id !== $user->id) {
              return $app->json('Esta terminal esta registrada a nombre de otro usuario',401);
            }
*/
            if ($pc->wasRecentlyCreated) {
              $fecha->add(new \DateInterval('P90D')); //add 90 dias
              $pc->expire = $fecha->format("Y-m-d H:i:s");
              $pc->user_id = $user->id;
              $pc->name = $pcname;
              $pc->save();
            } else {
              if(new \DateTime($pc->expire) < $fecha) {
                return $app->json('Esta terminal no tiene tiempo en el servidor',401);
              }
              // renovar el nombre del pc
              if($pc->name !== $pcname){
                $pc->name = $pcname;
                $pc->save();
              }
            }
            $user->access_token = bin2hex(random_bytes(16));
            if ($user->wasRecentlyCreated) {
              $user->password = password_hash ($pass, PASSWORD_DEFAULT);
              $user->expire = $fecha->format("Y-m-d H:i:s");
              $user->status = 1;
             
            } else if ($user && password_verify($pass, $user->password)) {
              
             
            } else {
              if(empty($user->password)) // no tiene pass
              {
                if(!empty($pass)) // pero le puso uno nuevo, le actualiza
                {
                  $user->password = password_hash ($pass, PASSWORD_DEFAULT);
                }
                
              } else {
                return $app->json('No se reconoce el password para esta cuenta',401);
              }
              //si no trae pass pasa
            }
            $user->save();
            
            return $app->json([
              'expire' => $pc->expire,
              'token' => $user->access_token,
              'negocio' => $user->name,
              'created' => $pc->created_at->format("Y-m-d H:i:s"),
              'terminal' => $pc->name


            ]); 
          }
          return $app->json('datos insuficientes',401);

        });
        $controllers->post('/negocio', function(Request $request, Application $app){
            /*$token=$request->request->get('token');
            if(empty($token)) {
              return $app->json('No tiene autorizacion',401);
            }*/
            $user = $app['user'];// User::loginWhitToken($token);
            if ($user) {
              $id = $request->request->get('Id');
              $name = $request->request->get('Nombre');
              $image = $request->request->get('image');
              if(empty($name)){
                return $app->json('No se asigno el nombre',400);
              }
              $param = $request->request->all();
              unset($param['token']);
              unset($param['imagen']);
              unset($param['error']);
              $negocio = null;
              $user->load('negocios');
              $isnew = false;
              if ($id) {
                $negocio = $user->negocios->first(function($value, $key) use ($id){
                  $val = $id == $value['Id'];
                  return $val;
                });
                if (empty($negocio)) {
                  return $app->json('Este negocio no le pertenece',400);
                }
                if($negocio->fill($param)) {
                  $negocio->saveImage($image);
                  $negocio->save();
                }
              } else {
                if (!$user->CanAddNegocio) {
                  return $app->json('No puede agregar un negocio en este momento',400);
                }
                $negocio = new Negocio($param);
                $negocio->user_id = $user->id;
                $negocio->saveImage($image);
                $negocio->generateCode();
                $negocio->save();

              }
              if($negocio) {
                return $app->json($negocio);
              }


            }
            return $app->json('No tiene autorizacion',401);

        });
        return $controllers;
    }

}
