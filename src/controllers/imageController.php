<?php

namespace controllers;

use Silex\Application;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Silex\Api\ControllerProviderInterface;

use models\Image;
use models\Pic;
class ImageController implements ControllerProviderInterface
{
  public function connect(Application $app)
    {
        $controllers = $app['controllers_factory'];
        $controllers->post('/upload', function(Request $request, Application $app){
          $user = $app['user'];
          if (!$user) {
            return $app->json('No tiene autorizacion',401);
          }
          $image = $request->files->get('image');
          $name = md5($user->email.uniqid()).'.jpg';
          if ($image) {
            if ($image->isValid()) {
              try {
                $image->move('./images/original', $name);
                return $app->json($name);
              } catch (Exception $e) {
                return $app->json('Se produjo un error al intentar guardar',400);
              }
            }
          }
          return $app->json('Se produjo un error',400);
        });
        $controllers->post('/pet', function(Request $request, Application $app){
          //check user
          $user = $app['user'];
          if (!$user) {
            return $app->json('No tiene autorizacion',401);
          }

          //check pet
          $petid = $request->request->get('petid');
          $user->load('pets');
          $pets = $user->pets;
          $pet = null;
          foreach ($pets as $p) {
            if ($petid == $p->id){
              $pet = $p;
              break;
            }

          }
          if (!$pet) {
            return $app->json('La mascota no le pertenece',400);
          }

          try {
            $data = $request->request->get('image64');
            list($type, $data) = explode(';', $data);
            list(, $data)      = explode(',', $data);
            $image = base64_decode($data);
            $name = md5($user->email.uniqid()).'.jpg';
            $description =  $request->request->get('description');

            file_put_contents('./images/original/'.$name, $image);
            $pic = new Pic([
              'name' => $name,
              'description' => $description,
              'pet_id' => $petid,
              'user_id' => $user->id
            ]);
            if($pic->save()) {
              return  $app->json($pic);
            }
          } catch (Exception $e) {
            return $app->json('Se produjo un error al intentar guardar',400);
          }
          
          return $app->json('Se produjo un error',400);

        });
        $controllers->get('/{size}_{image}', function(Request $request, Application $app){
          $name = $request->attributes->get('image');
          $size = $request->attributes->get('size');
          $img = new Image($name, $size);
          header("Content-Type: image/jpg");
      		header('Cache-Control: private, max-age=14400');

      		header('Expires: ' . gmdate('D, d M Y H:i:s', time() + 14400) . ' GMT');
      		$r = file_get_contents($img->path());
      		header('Etag: ' . md5($r));

      		die($r);

        });
        $controllers->get('/{image}', function(Request $request, Application $app){
          $name = $request->attributes->get('image');
          $img = new Image($name, false);
          header("Content-Type: image/jpg");
      		header('Cache-Control: private, max-age=14400');
      		header('Expires: ' . gmdate('D, d M Y H:i:s', time() + 14400) . ' GMT');
      		$r = file_get_contents($img->path());
      		header('Etag: ' . md5($r));

      		die($r);

        });
        return $controllers;
    }

}
