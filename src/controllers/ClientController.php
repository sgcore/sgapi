<?php
namespace controllers;

use Silex\Application;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Silex\Api\ControllerProviderInterface;
use models\Product;
use clientes\Cliente;
use models\Image;
use models\User;
class ClientController implements ControllerProviderInterface
{
  public function connect(Application $app)
  {
      $controllers = $app['controllers_factory'];
      // midleware before
      $controllers->before(function(Request $request) use ($app) {
        $client = $request->attributes->get("client");
        if(!Cliente::validClient($client)) {
         // "short-circuit" the request-handling (see documentation)
         return new Response("", 404);
        }
        $app["client"] = $client;
        $token = null;
        $token = $request->request->get('clitoken');
        if (!$token) {
          $token = $request->query->get('clitoken');
        }
        
        if($token)
        {
          $user = User::on($client)->where('access_token','=',$token)->first();
          if($user)
          {
            $app['user'] = $user;
          }
          else {
            return $app->json('No tiene autorizacion',401);
          }

        }
        


      });
      $controllers->post('/info', function (Request $request, Application $app){
        $user = $app['user'];
        if (!$user || $user->role != "Admin") {
          return $app->json('No tiene autorizacion',401);
        }
        $cliente = $request->attributes->get("client");
        $ret = [
          'code'=> $cliente,
          'name' => $request->request->get('name'),
          'description' => $request->request->get('description'),
          'logo' => str_replace("info","images/logo.png","http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"),

        ];
        
        $image =  $request->files->get('image');
        if(!$image){
          $image =  $request->files->get('file');
        }
        if ($image) {
          if ($image->isValid()) {
            try {
              $image->move(Cliente::pathClientFolder($cliente,'images/original/'), 'logo.png');
             
            } catch (Exception $e) {
              return $app->json('Se produjo un error al intentar guardar',400);
            }

          }
        }
        $error = Cliente::saveClientInfo($ret);
        if($error)
        {
          return $app->json($error, 400);
        }
        return $app->json($ret);

       
        
       
      });

      /**
       * Obtiene la informacion del cliente
       */
      $controllers->get('/info', function(Request $request, Application $app){
        return $app->json(Cliente::getClientInfo($app["client"]));

      });

      //images
      // Imagen original
      $controllers->get('/images/{image}', function(Request $request, Application $app){
        $name = $request->attributes->get('image');
        $cliente = $request->attributes->get("client");
        $porciones = explode("_", $name);
        $size = false;
        if(is_array($porciones) && count($porciones) == 2){
          $size = $porciones[0];
          $name = $porciones[1];
        }
        $img = new Image($name, $size, $cliente); //cliente pets
        header("Content-Type: image/jpg");
        header('Cache-Control: private, max-age=14400');
        header('Expires: ' . gmdate('D, d M Y H:i:s', time() + 14400) . ' GMT');
        $r = file_get_contents($img->path());
        header('Etag: ' . md5($r));

        die($r);

      });


     
      // Productos (todo tipo)
      $controllers->get('/products', function (Request $request, Application $app){
        $cliente = $request->attributes->get("client");
        $user = $app['user'];
        $owner = $user && $user->ValidateOwnerCliente($cliente);
        
        /**
         * all: Muestra todo
         * basic: lo basico
         * price: solo nombre codigo y precio
         * extra: todo menos caracteristicas
         * carac: todo menos costo
         * costo: lo basico mas el costo
         * 
         */
        $show = $request->query->get('show');
       

        switch($show)
        {
          case 'all':
            Product::showAll($owner);
            break;
          case 'basic':
            Product::showBasic();
            break;
          case 'price':
            Product::showPrecio();
            break;
          case 'extra':
            Product::showExtra();
            break;
          case 'carac':
            Product::showCaract();
            break;
          case 'costo':
            Product::showCosto($owner);
            break;
          
        }
        $prods = Product::on($cliente)->get();
        
        return $app->json($prods);
      });
      $controllers->get('/eapiprods', function (Request $request, Application $app){
        $cliente = $request->attributes->get("client");
        Product::showEapi();
        $prods = Product::on($cliente)->get();
        return $app->json($prods);
      });
            
      // Upload producto
      $controllers->post('/product', function(Request $request, Application $app){
        $user = $app['user'];
        if (!$user) {
          return $app->json('No tiene autorizacion',401);
        }
        $cliente = $request->attributes->get("client");
        if(!$user->ValidateOwnerCliente($cliente))
        {
          return $app->json('No tiene autorizacion para modificar un producto en '.$cliente,401);
        }

        $codigo = $request->request->get('Codigo');
        $product = $user->getProducto($codigo, true, true);
        if(!$product)
        {
          return $app->json('No se ingreso correctamente el codigo del producto',400);
        }
        /*
        if($product->trashed()){
          $product->restore();
        }*/
        if($product->UpdateFromRequest($request))
        {
          if($product->image)
          { //check image exist
            $myimg = new Image($product->image,null,$cliente);
            if(!$myimg->isValid()){
             $product->image = null;
            }
          }
          $product->save();
          
          Product::showAll($user);
          return $app->json($product);
        }
        return $app->json($codigo.' no tiene cambios registrados para sincronizar',400);

      });

       // Upload producto
       $controllers->post('/prodimg', function(Request $request, Application $app){
        $user = $app['user'];
        if (!$user) {
          return $app->json('No tiene autorizacion',401);
        }
        $cliente = $request->attributes->get("client");
        if(!$user->ValidateOwnerCliente($cliente))
        {
          return $app->json('No tiene autorizacion para modificar un producto en '.$cliente,401);
        }

        $codigo = $request->headers->get('Codigo');
        $product = $user->getProducto($codigo, false, true);
        if(!$product)
        {
          return $app->json('No se ingreso correctamente el codigo del producto',400);
        }

        $image = $request->files->get('file');
        //$name = md5($user->email.uniqid()).'.jpg';
        $name = md5($codigo.$product->id).'.jpg';
        
        if ($image) {
          if ($image->isValid()) {
            try {
              $destination = $user->pathToOriginalImage();
              $moved = $image->move($destination, $name);
              $new = new Image($name,false,$cliente);
              $iname = $product->image;
              if($iname){
                  $old = new Image($iname,false,$cliente);
                  $old->cleanCache();
              }
              
              if($new->isValid()){
                  $product->image = $new->name();
                  $product->update(["image"=>$name]);
                  return $app->json(["image" => $name]);
              }
              
            } catch (Exception $e) {
              return $app->json('Se produjo un error al intentar guardar',400);
            }
          }
        }
        
        return $app->json('Se produjo un error con la imagen del producto',400);
      });
      // remove producto
      $controllers->post('/remprod', function(Request $request, Application $app){
        $user = $app['user'];
        if (!$user) {
          return $app->json('No tiene autorizacion',401);
        }
        $cliente = $request->attributes->get("client");
        if(!$user->ValidateOwnerCliente($request->attributes->get("client")))
        {
          return $app->json('No tiene autorizacion para modificar un producto en '.$cliente,401);
        }

        $codigo = $request->request->get('Codigo');
        $product = $user->getProducto($codigo);
        if(!$product)
        {
          return $app->json('No se encontro el producto que quiere desincronizar',400);
        }
        /*
        $img = $product->image;
        $new = new Image($img,false,$cliente);
        if($new){
          $new->deleteMe();
        }
        */
        if($product->delete()){
          return $app->json('Se elimino correctamente');
        }
        return $app->json('No se pudo eliminar el producto',400);
      });
      // Imagen original
      $controllers->get('/images/{image}', function(Request $request, Application $app){
        $cliente = $request->attributes->get("client");
        $name = $request->attributes->get('image');
        $porciones = explode("_", $name);
        $size = false;
        if(is_array($porciones) && count($porciones) == 2){
          $size = $porciones[0];
          $name = $porciones[1];
        }
        $img = new Image($name, $size, $cliente);
        header("Content-Type: image/jpg");
        header('Cache-Control: private, max-age=14400');
        header('Expires: ' . gmdate('D, d M Y H:i:s', time() + 14400) . ' GMT');
        $r = file_get_contents($img->path());
        header('Etag: ' . md5($r));

        die($r);

      });

      $controllers->post('/images/{code}', function(Request $request, Application $app){
        $user = $app['user'];
        if (!$user) {
          return $app->json('No tiene autorizacion',401);
        }
        $cliente = $request->attributes->get("client");
        if(!$user->ValidateOwnerCliente($request->attributes->get("client")))
        {
          return $app->json('No tiene autorizacion para modificar un producto en '.$cliente,401);
        }

        $codigo = $request->request->get('Codigo');
        // busca el producto
        $product = $user->getProducto($codigo);
        if(!$product)
        {
          return $app->json('No se ingreso correctamente el codigo del producto',400);
        }

        $image = $request->files->get('image');
        $name = md5($user->email.uniqid()).'.jpg';
        if ($image) {
          if ($image->isValid()) {
            try {
              $image->move($user->pathToOriginalImage(), $name);
              $product->image = $name;
              $product->save();
              return $app->json($name);
            } catch (Exception $e) {
              return $app->json('Se produjo un error al intentar guardar',400);
            }
          }
        }
        return $app->json('Se produjo un error',400);
      });
      return $controllers;
  }

}
