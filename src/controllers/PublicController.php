<?php
namespace controllers;

use Silex\Application;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Silex\Api\ControllerProviderInterface;
use clientes\Cliente;
class PublicController implements ControllerProviderInterface
{
  public function connect(Application $app)
  {
      $controllers = $app['controllers_factory'];
      $controllers->get('/customers', function(Request $request, Application $app){
          $dirs = Cliente::allClients();
          $ret = [];
          foreach($dirs as $pt)
          {
              $str = substr($pt, strrpos($pt, "/") + 1); 
              if($str !== "pets")
              {
                   $ret[] = $str;
              }
           
          }
        return $app->json($ret);

      });
      return $controllers;
  }

}
