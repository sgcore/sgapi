<?php
use controllers\ProductController;
use controllers\MovimientoController;
use controllers\UserController;
use controllers\ClientController;
use controllers\ClientUserController;
use controllers\ClientOfficeController;
use controllers\ClientSaleController;
use controllers\ClientProductController;
use controllers\ClientTagController;
use controllers\PublicController;
use controllers\HomeController;
use controllers\PetController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use models\User;
use models\Negocio;
use clientes\Cliente;


$whitelist = array(
  '127.0.0.1',
  '::1'
);
$localhost = in_array($_SERVER['REMOTE_ADDR'], $whitelist);
$debug =  false; //$localhost;
if($debug){
  error_reporting(E_ALL);
  ini_set('display_errors', 'On');
  $app['debug'] = true;
}

$loader = require __DIR__.'/vendor/autoload.php';
$app = new Silex\Application();
$cnn = require __DIR__.'/src/database/remote.php';//database connections string

if($localhost){
 
  $cnn = require __DIR__.'/src/database/local.php';//database connections string
}

$app->register(new Silex\Provider\MonologServiceProvider(), array(
  'monolog.logfile' => __DIR__.'/development.log',
  'monolog.'
));
$app->register(new JDesrosiers\Silex\Provider\CorsServiceProvider(), [
  //"cors.allowOrigin" => "http://petstore.swagger.wordnik.com",
]);
$app['monolog']->addDebug('Testing the Monolog logging.');

// Debug Sesion
if($debug)
{
  $app['debug'] = true;
}

//conections
$clientes = Cliente::allClients();
$ccnn = ['default' => $cnn];
if(!empty($clientes)){
  foreach ($clientes as $path) {
    $key = basename ($path);
    $ccnn[$key] =  [
        'driver' => 'sqlite',
        'database'  => './clientes/'.$key.'/sync.db',
        'prefix' => '',
      ];
  }
}


$app->register(
    new \JG\Silex\Provider\CapsuleServiceProvider(),
    [
        'capsule.connections' => $ccnn
    ]
);

$app->before(function (Request $request) use ($app) {
  if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
    $data = json_decode($request->getContent(), true);
    $request->request->replace(is_array($data) ? $data : array());
  }
  $token =  $request->request->get('token');
  if(empty($token)){
    $token = $request->query->get('token');
  }
  if(empty($token)){
    $token = $request->headers->get('token');
  }
  $app['user'] = null;
  if($token) {
    $user = User::loginWhitToken($token);
    if($user) {
      $app['user'] = $user;
      $negid=$request->request->get('negocio');
      if($negid) {
        $negocio =$user->Mynegocio($negid);
        if($negocio){
          $app['negocio'] = $negocio;
          $path = Cliente::getNegocioPath($negocio);
          $connections = $app['capsule.connections'];
          $connections[$negocio->Codigo] = [
              'driver' => 'sqlite',
              'database'  => $path.'sync.db',
              'prefix' => '',
          ];
          $app['capsule.connections'] = $connections;
        } else {
          return $app->json([ 'message'=>'No tiene acceso al negocio'],401);
        }
      }
    } else {
      return $app->json('Token invalido',401);
    }

  }


});
//cors
$app->after(function (Request $request, Response $response) {
    $response->headers->set('Access-Control-Allow-Origin', '*');
    $response->headers->set('Access-Control-Allow-Headers', '*');
    $response->headers->set('Access-Control-Allow-Methods', '*');
    $response->headers->set('Access-Control-Allow-Credentials', 'true');

});

//Moun Controllers
/*$app->mount('/products', new ProductController());
$app->mount('/movimientos', new MovimientoController());*/
$app->mount('/', new HomeController());
$app->mount('/users', new UserController());
$app->mount('/pets', new PetController());
$app->mount('/public', new PublicController());
$app->mount('/{client}/users', new ClientUserController());
$app->mount('/{client}/offices', new ClientOfficeController());
$app->mount('/{client}/products', new ClientProductController());
$app->mount('/{client}/sales', new ClientSaleController());
$app->mount('/{client}/tags', new ClientTagController());
$app->mount('/{client}', new ClientController());
$app["cors-enabled"]($app);

//Execute app
$app->run();
